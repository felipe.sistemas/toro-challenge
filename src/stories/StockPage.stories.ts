import { MatTableDataSource } from '@angular/material/table';
import { Component } from '@angular/core';
import { MaterialModule } from './../app/modules/material/material.module';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { StockOrderComponent } from './../app/components/reusable/stock-order/stock-order.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { StockService } from './../app/shared/services/stock/stock.service';
import { StockListComponent } from '../app/components/reusable/stock-list/stock-list.component';
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { Stock } from 'src/app/shared/services/stock/stock';
import { within } from '@storybook/testing-library';
import { Overlay } from '@angular/cdk/overlay';

class MockStockService implements Partial<StockService> {
  static getTrendingStocks(): Stock[] | undefined {
    return [
      new Stock('PETR4', 28.44),
      new Stock('MGLU3', 25.91)
    ];
  }
}

export default {
  title: 'Example/StockPage',
  component: StockListComponent,
  parameters: {
    storyshots: { disable: true },
    chromatic: { disable: true },
  },
  decorators: [
    moduleMetadata({
      declarations: [StockOrderComponent],
      imports: [CommonModule, MaterialModule],
      providers: [
        MockStockService,
        HttpClient,
        HttpHandler,
        MatDialog,
        Overlay,
        MatSnackBar,
      ]
    }),
  ],
} as Meta;


const Template: Story<StockListComponent> = (args: StockListComponent) => ({
  props: args,
});

export const Empty = Template.bind({});

export const NotEmpty: Story<StockListComponent> = () => ({
  props: {isLoadingResults: false, data: new MatTableDataSource<Stock>(MockStockService.getTrendingStocks())}
});
