import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/reusable/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './modules/material/material.module';
import { StockListComponent } from './components/reusable/stock-list/stock-list.component';
import { TransactionFormComponent } from './components/reusable/transaction-form/transaction-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TransactionWrapperComponent } from './components/transaction-wrapper/transaction-wrapper.component';
import { AccountComponent } from './components/reusable/account/account.component';
import { NgxMaskModule } from 'ngx-mask';
import { StockOrderComponent } from './components/reusable/stock-order/stock-order.component';
import { StockWrapperComponent } from './components/stock-wrapper/stock-wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    StockListComponent,
    TransactionFormComponent,
    TransactionWrapperComponent,
    AccountComponent,
    StockOrderComponent,
    StockWrapperComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot({
      validation: true,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
