import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Account } from 'src/app/shared/services/account/account';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { AbstractComponent } from '../reusable/abstraction/abstract-component';
import { Response } from '../../shared/common/response';

@Component({
  selector: 'app-stock-wrapper',
  templateUrl: './stock-wrapper.component.html',
  styleUrls: ['./stock-wrapper.component.scss']
})
export class StockWrapperComponent extends AbstractComponent implements OnInit {

  account!: Account;
  isLoadingResults: boolean = true;
  isRateLimitReached: boolean = false;

  constructor(
    private accountService: AccountService,
    _snackBar: MatSnackBar
  ) {
    super(_snackBar);
  }

  ngOnInit() {
    this.getAccount();
  }

  getAccount() {
    this.accountService.getAccountByNumber("572928").subscribe((response: Response<Account>) => {
      if (!response.success) return response.errors.forEach(error => this.openSnackBar(error, 'Close'));

      this.account = new Account(response.data.bank, response.data.branch, response.data.number, response.data.balance, response.data.createdOn, response.data.user);
      this.isLoadingResults = false;
    });
  }

}
