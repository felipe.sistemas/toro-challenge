import { AccountService } from './../../shared/services/account/account.service';
import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/shared/services/account/account';
import { Response } from '../../shared/common/response';
import { AbstractComponent } from '../reusable/abstraction/abstract-component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-transaction-wrapper',
  templateUrl: './transaction-wrapper.component.html',
  styleUrls: ['./transaction-wrapper.component.scss']
})
export class TransactionWrapperComponent extends AbstractComponent implements OnInit {

  account!: Account;
  isLoadingResults: boolean = true;
  isRateLimitReached: boolean = false;

  constructor(
    private accountService: AccountService,
    _snackBar: MatSnackBar
  ) {
    super(_snackBar);
  }

  ngOnInit() {
    this.getAccount();
  }

  getAccount() {
    this.accountService.getAccountByNumber("572928").subscribe((response: Response<Account>) => {
      if (!response.success) return response.errors.forEach(error => this.openSnackBar(error, 'Close'));

      this.account = new Account(response.data.bank, response.data.branch, response.data.number, response.data.balance, response.data.createdOn, response.data.user);
      this.isLoadingResults = false;
    });
  }

}
