import { StockService } from 'src/app/shared/services/stock/stock.service';
import { StockOrder } from './../../../shared/common/stock-order';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AbstractComponent } from '../abstraction/abstract-component';
import { Order } from 'src/app/shared/services/stock/order';
import { Response } from '../../../shared/common/response';

@Component({
  selector: 'app-stock-order',
  templateUrl: './stock-order.component.html',
  styleUrls: ['./stock-order.component.scss']
})
export class StockOrderComponent extends AbstractComponent implements OnInit {

  stockOrderForm!: FormGroup;

  constructor(
    public fb: FormBuilder,
    _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<StockOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: StockOrder,
    private stockService: StockService
  ) {
    super(_snackBar)
  }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.stockOrderForm = this.fb.group({
      amount: [1, Validators.required]
    });
  }

  public handleError = (controlName: string, errorName: string) => this.stockOrderForm.controls[controlName].hasError(errorName);

  submit() {
    if (this.stockOrderForm.valid && this.affordable()) {
      this.stockService.createOrder(new Order(this.data.stock.symbol, this.stockOrderForm.value.amount, this.data.account.number)).subscribe((response: Response<Order>) => {
        if(!response.success) return response.errors.forEach(error => this.openSnackBar(error, 'Close'));

        this.openSnackBar('Success', 'Close');
        this.data.orderSucceeded.emit(true);
        
        this.resetForm();
        this.dialogRef.close();
      });
    }
  }

  public total = () => this.data.stock.currentPrice * this.stockOrderForm.value.amount;
  public affordable = () => this.data.account.balance > this.total();

  resetForm() {
    this.stockOrderForm.reset();

    Object.keys(this.stockOrderForm.controls).forEach(key => {
      this.stockOrderForm.controls[key].setErrors(null)
    });

    this.createForm();
  }

}
