import { Account } from './../../../shared/services/account/account';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  @Input() account!: Account;
  @Input() isLoadingResults: boolean = true;
  @Input() isRateLimitReached: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }
  
}
