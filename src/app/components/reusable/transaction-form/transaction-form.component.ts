import { TransactionService } from './../../../shared/services/transaction/transaction.service';
import { AccountTransaction, Transaction } from './../../../shared/services/transaction/transaction';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TransactionType } from 'src/app/shared/services/transaction/transaction-enum';
import { AbstractComponent } from '../abstraction/abstract-component';

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.scss']
})
export class TransactionFormComponent extends AbstractComponent implements OnInit {

  transactionForm!: FormGroup;
  transactionTypes: Array<string> = new Array("PIX", "TED");
  @Output() transactionSucceeded: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    public fb: FormBuilder,
    _snackBar: MatSnackBar,
    private transactionService: TransactionService
  ) {
    super(_snackBar);
   }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.transactionForm = this.fb.group({
      number: ['', [Validators.required]],
      type: ['', [Validators.required]],
      bank: ['352', [Validators.required]],
      branch: ['0001', [Validators.required]],
      cpf: ['', Validators.required],
      amount: [0, Validators.required]
    });
  }

  public handleError = (controlName: string, errorName: string) => this.transactionForm.controls[controlName].hasError(errorName);

  resetForm() {
    this.transactionForm.reset();

    Object.keys(this.transactionForm.controls).forEach(key => {
      this.transactionForm.controls[key].setErrors(null)
    });

    this.createForm();
  }

  submit() {
    if (this.transactionForm.valid) {
      var value = this.transactionForm.value;

      var transaction = new Transaction(TransactionType.TRANSFER, value.amount,
        new AccountTransaction(value.cpf, value.number, value.branch, value.bank),
        new AccountTransaction(value.cpf, value.number, value.branch, value.bank));

      this.transactionService.createTransaction(transaction).subscribe(result => {
        if (result.success) {
          this.openSnackBar('Success', 'Close');
          this.transactionSucceeded.emit(true);
        } else {
          result.errors.forEach(x => this.openSnackBar(x, 'Close'));
        }

      });

      this.resetForm();
    }
  }

}
