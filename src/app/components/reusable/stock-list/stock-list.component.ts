import { StockOrderComponent } from './../stock-order/stock-order.component';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Stock } from 'src/app/shared/services/stock/stock';
import { StockService } from 'src/app/shared/services/stock/stock.service';
import { Account } from 'src/app/shared/services/account/account';
import { StockOrder } from 'src/app/shared/common/stock-order';
import { AbstractComponent } from '../abstraction/abstract-component';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.scss']
})
export class StockListComponent extends AbstractComponent implements OnInit {

  isLoadingResults: boolean = true;
  isRateLimitReached: boolean = false;
  @Input() account!: Account;
  @Output() orderSucceeded: EventEmitter<boolean> = new EventEmitter<boolean>();

  data!: MatTableDataSource<Stock>;
  public displayedColumns: string[] = ['symbol', 'currentPrice', 'action'];

  constructor(
    private stockService: StockService,
    private dialog: MatDialog,
    _snackBar: MatSnackBar) {
      super(_snackBar);
  }

  ngOnInit() {
    this.stockService.getTrendingStocks().subscribe(x => {
      this.data = new MatTableDataSource<Stock>(x);
      this.isLoadingResults = false;
    });
  }

  createStockOrder(stock: Stock) {
    const dialogRef = this.dialog.open(StockOrderComponent, {
      maxWidth: "800px",
      data: new StockOrder(stock, this.account, this.orderSucceeded)
    });

    dialogRef.afterClosed().subscribe(res => {
    })
  }


}
