import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({ template: '' })
export abstract class AbstractComponent {

  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  protected openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
