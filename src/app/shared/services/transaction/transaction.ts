export class Transaction {
    public event: string;
    public amount: number;
    public target: AccountTransaction;
    public origin: AccountTransaction;

    constructor(event: string, amount: number, target: AccountTransaction, origin: AccountTransaction) {
        this.event = event;
        this.amount = amount;
        this.target = target;
        this.origin = origin;
    }

}

export class AccountTransaction {
    public cpf?: string;
    public account: string;
    public branch: string;
    public bank: string;

    constructor(cpf: string, account: string, branch: string, bank: string) {
        this.cpf = cpf;
        this.account = account;
        this.branch = branch;
        this.bank = bank;
    }
}