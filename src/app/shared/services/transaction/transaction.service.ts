import { Transaction } from './transaction';
import { Response } from '../../common/response';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) { }

  createTransaction(transaction: Transaction){
    return this.http.post<Response<Transaction>>(`${environment.apiBaseUrl}/sbp/events`, transaction);
  }
}
