export class User {
    public name: string;
    public cpf: string;

    constructor(cpf: string, name: string) {
        this.name = name;
        this.cpf = cpf;
    }
}