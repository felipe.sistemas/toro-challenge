import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Account } from './account';
import { Response } from '../../common/response';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getAccountByNumber(number: string){
    return this.http.get<Response<Account>>(`${environment.apiBaseUrl}/account/${number}`);
  }
}
