import { User } from "../user/user";

export class Account {
    public bank: string;
    public branch: string;
    public number: string;
    public balance: number;
    public createdOn: Date;
    public user!: User;

    constructor(bank: string, branch: string, number: string, balance: number, createdOn: Date, user: User) {
        this.bank = bank;
        this.branch = branch;
        this.number = number;
        this.balance = balance;
        this.createdOn = createdOn;
        this.user = new User(user.cpf, user.name);
    }

    public fullAccount(): string {
        return `${this.branch} - ${this.number}`
    }
}