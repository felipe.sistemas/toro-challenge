export class Order {
    public symbol: string;
    public amount: number;
    public accountNumber: string;

    constructor(symbol: string, amount: number, accountNumber: string){
        this.symbol = symbol;
        this.amount = amount;
        this.accountNumber = accountNumber;
    }
}