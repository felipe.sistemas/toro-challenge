export class Stock {
    public symbol: string;
    public currentPrice: number;

    constructor(symbol: string, currentPrice: number) {
        this.symbol = symbol;
        this.currentPrice = currentPrice;
    }
}