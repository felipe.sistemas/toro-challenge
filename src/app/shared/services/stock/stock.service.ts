import { environment } from './../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Stock } from './stock';
import { Order } from './order';
import { Response } from './../../common/response';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http: HttpClient) { }

  getTrendingStocks(){
    return this.http.get<Array<Stock>>(`${environment.apiBaseUrl}/stock/trends`);
  }

  createOrder(order: Order){
    return this.http.post<Response<Order>>(`${environment.apiBaseUrl}/order`, order);
  }
}
