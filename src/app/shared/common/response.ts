export class Response <T> {
    public success: boolean;
    public data: T;
    public errors: Array<string>;

    constructor(success: boolean, data: T, errors: Array<string>){
        this.success = success;
        this.data = data;
        this.errors = errors;
    }
}