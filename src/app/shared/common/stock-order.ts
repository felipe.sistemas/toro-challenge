import { EventEmitter } from '@angular/core';
import { Account } from './../services/account/account';
import { Stock } from './../services/stock/stock';
export class StockOrder {
    public stock: Stock;
    public account: Account;
    public orderSucceeded: EventEmitter<boolean>

    constructor(stock: Stock, account: Account, orderSucceeded: EventEmitter<boolean>) {
        this.stock = stock;
        this.account = account;
        this.orderSucceeded = orderSucceeded;
    }
}