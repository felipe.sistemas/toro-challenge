import { StockWrapperComponent } from './components/stock-wrapper/stock-wrapper.component';
import { TransactionWrapperComponent } from './components/transaction-wrapper/transaction-wrapper.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'stock', component: StockWrapperComponent,
  },
  {
    path: 'transaction', component: TransactionWrapperComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
